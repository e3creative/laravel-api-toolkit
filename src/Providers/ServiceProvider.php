<?php

namespace E3Creative\LaravelApiToolkit\Providers;

use Illuminate\Support\ServiceProvider as IlluminateServiceProvider;

class ServiceProvider extends IlluminateServiceProvider
{
    /**
     * @var string
     */
    private $directories = [
        'config'     => __DIR__ . '/../../config',
        'migrations' => __DIR__ . '/../../migrations',
        'routes'     => __DIR__ . '/../../routes',
    ];

    /**
     * Perform post-registration booting of services.
     *
     * @return void
     */
    public function boot()
    {
        $this->autoloadConfigFiles();
        $this->autoloadRouteFiles();

        $this->loadMigrationsFrom($this->directories['migrations']);
    }

    /**
     * Grabs all the files out of the config directory and registers them with
     * Laravel's package asset publisher
     *
     * @return void
     */
    private function autoloadRouteFiles()
    {
        if ($files = $this->getFilesFromDirectory($this->directories['routes'])) {
            foreach ($files as $file) {
                $this->loadRoutesFrom($file);
            }
        }
    }

    /**
     * Grabs all the files out of the config directory and registers them with
     * Laravel's package asset publisher
     *
     * @return void
     */
    private function autoloadConfigFiles()
    {
        if ($files = $this->getFilesFromDirectory($this->directories['config'])) {
            $destinations = array_map(function ($item) {
                $parts = explode('/', $item);
                return config_path(array_pop($parts));
            }, $files);

            $this->publishes(array_combine($files, $destinations));
        }
    }

    /**
     * Return an array of files inside a given directory
     *
     * @param  string $directory
     * @return array
     */
    private function getFilesFromDirectory(string $directory)
    {
        if (file_exists($directory)) {
            $files = array_map(
                function ($file) use ($directory) {
                    return sprintf('%s/%s', $directory, $file);
                },
                array_slice(scandir($directory), 2)
            );

            return $files;
        }
    }
}
